module.exports = {
  title: "TLog",
  tagline: "轻量级的分布式日志标记追踪神器",
  url: "http://tlog.yomahub.com",
  baseUrl: "/",
  onBrokenLinks: "throw",
  favicon: "img/favicon.ico",
  organizationName: "yomahub", // Usually your GitHub org/user name.
  projectName: "tlog", // Usually your repo name.
  scripts: [
    '/script/baidu.js',
    'https://cdn.wwads.cn/js/makemoney.js',
    '/script/adscheck.js'
  ],
  themeConfig: {
    colorMode: {
      defaultMode: 'dark',
      disableSwitch: true,
      respectPrefersColorScheme: false,
      switchConfig: {
        darkIcon: "🌙",
        darkIconStyle: {
          marginLeft: "2px",
        },
        lightIcon: "☀️",
        lightIconStyle: {
          marginLeft: "1px",
        },
      },
    },
    prism: {
      darkTheme: require("prism-react-renderer/themes/dracula"),
      additionalLanguages: ['java']
    },
    image: "img/tlog.png",
    announcementBar: {
      id: "github-star",
      content:
          '<font style="font-size: medium; font-weight: bolder">⭐️ If you like TLog,<a target="_blank" style="font-size: medium; font-weight: bolder" rel="noopener noreferrer" href="https://github.com/dromara/TLog">please give us a star on GitHub! ⭐️</a></font>',
      backgroundColor: '#ffce69',
      textColor: '#212122',
      isCloseable: false,
    },
    navbar: {
      hideOnScroll: false,
      title: "TLog",
      logo: {
        alt: "TLog Logo",
        src: "img/logo.svg",
      },
      items: [
        {
          label: "文档",
          position: "left",
          to: "docs",
          activeBasePath: "docs",
        },
        {
          label: '更新记录',
          position: 'left',
          to: 'blog/update-log'
        },
        {
          label: '关于作者',
          position: 'left',
          to: 'blog/about-author'
        },
        {
          label: '加入讨论群',
          position: 'left',
          to: 'blog/group-chat'
        },
        {
          label: '赞助',
          position: 'left',
          to: 'blog/donation'
        },
    	{
          type: "localeDropdown", position: "right",
        },
        {
          label: '看板',
          href: 'https://gitee.com/dromara/TLog/board',
          className: "navbar-item-board",
          position: 'right',
        },
        {
          href: "https://gitee.com/dromara/TLog",
          label: "Gitee",
          className: "navbar-item-gitee",
          position: "right",
        },
        {
          href: "https://github.com/dromara/TLog",
          label: "GitHub",
          className: "navbar-item-github",
          position: "right",
        },
      ],
    },
    footer: {
      copyright: `The MIT License (MIT) | Copyright © ${new Date().getFullYear()} YOMAHUB`,
    },
  },
  plugins: ["docusaurus-plugin-sass", "@docusaurus/plugin-ideal-image"],
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl: "https://gitee.com/dromara/tlog-website/tree/master/",
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl: "https://gitee.com/dromara/tlog-website/tree/master/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.scss"),
        },
      },
    ],
  ],
  i18n: {
    defaultLocale: "zh-cn",
    locales: ["zh-cn", "en"],
    localeConfigs: {
      "zh-cn": {
        label: "中文（中国）",
        direction: "ltr",
      },
      en: {
        label: "English",
        direction: "ltr",
      },
    },
  },
};
