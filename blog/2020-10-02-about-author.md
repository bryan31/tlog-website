---
slug: about-author
title: 关于作者
author: 铂赛东
author_title: 架构师 & 开源作者 & 内容创作者
author_image_url: /img/author.png
---

我是一个开源作者，也是一名内容创作者。深耕Java后端十多年，专注于架构，开源，微服务，分布式等领域的技术研究和原创分享。

TLog是我最近的一个开源项目，个人维护，且一直会迭代维护下去。如果喜欢这个项目，请在Gitee，Github上为我这个项目点上star，个人做开源不易，希望能得到你的支持和鼓励！

TLog的Gitee仓库：https://gitee.com/dromara/TLog

TLog的Github仓库：https://github.com/dromara/TLog

你也可以关注我的公众号，我会分享实用技术，梳理知识，一般周更，在技术的道路上和你一起成长进步

![img](/img/offical-wx.jpg)
