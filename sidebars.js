module.exports = {
  docs: [
    {
      type: "doc",
      id: "getting-started",
    },
    {
      type: "doc",
      id: "features",
    },
    {
      type: "doc",
      id: "select-type",
    },
    {
      type: "category",
      label: "安装TLog",
      collapsed: true,
      items: [
        "dependency/dependency-whole",
        "dependency/dependency-ondemand"
      ],
    },
    {
      type: "category",
      label: "Javaagent方式",
      collapsed: true,
      items: [
        "javaagent-type/javaagent-type-info",
        "javaagent-type/javaagent-type-shell-config",
        "javaagent-type/javaagent-type-log-result",
      ],
    },
    {
      type: "category",
      label: "字节码注入方式",
      collapsed: true,
      items: [
        "bytes-type/bytes-type-info",
        "bytes-type/bytes-type-injection",
      ],
    },
    {
      type: "category",
      label: "日志框架适配方式",
      collapsed: true,
      items: [
        "log-adaptor-type/log-adaptor-type-info",
        "log-adaptor-type/log-adaptor-type-log4j",
        "log-adaptor-type/log-adaptor-type-logback",
        "log-adaptor-type/log-adaptor-type-log4j2",
      ],
    },
    {
      type: "doc",
      id: "log-template",
    },
    {
      type: "category",
      label: "标签位置的自定义(MDC模式)",
      collapsed: true,
      items: [
        "mdc/mdc-info",
        "mdc/mdc-log4j",
        "mdc/mdc-logback",
        "mdc/mdc-log4j2",
      ],
    },
    {
      type: "doc",
      id: "spanid-rule",
    },
    {
      type: "category",
      label: "业务标签",
      collapsed: true,
      items: [
        "custom-label/custom-label-info",
        "custom-label/custom-label-simple-example",
        "custom-label/custom-label-multi-args",
        "custom-label/custom-label-multi-joint",
        "custom-label/custom-label-str",
        "custom-label/custom-label-dot-operation",
        "custom-label/custom-label-convertor",
      ],
    },
    {
      type: "category",
      label: "异步线程支持",
      collapsed: true,
      items: [
        "async-thread/async-thread-common",
        "async-thread/async-thread-pool",
        "async-thread/async-thread-mdc",
      ],
    },
    {
      type: "doc",
      id: "httpclient",
    },
    {
      type: "doc",
      id: "okhttp",
    },
    {
      type: "doc",
      id: "hutool-http",
    },
    {
      type: "doc",
      id: "servlet",
    },
    {
      type: "doc",
      id: "forest",
    },
    {
      type: "doc",
      id: "resttemplate",
    },
    {
      type: "doc",
      id: "mq",
    },
    {
      type: "doc",
      id: "spring-cloud-gateway",
    },
    {
      type: "doc",
      id: "soul",
    },
    {
      type: "category",
      label: "任务框架的支持",
      collapsed: true,
      items: [
        "biz-task/biz-task-jdk",
        "biz-task/biz-task-quartz",
        "biz-task/biz-task-spring-scheduled",
        "biz-task/biz-task-xxljob",
      ],
    },
    {
      type: "category",
      label: "Logstash的支持",
      collapsed: true,
      items: [
        "logstash/logstash-logback",
      ],
    },
    {
      type: "doc",
      id: "invoke-time",
    },
    {
      type: "doc",
      id: "custom_traceid",
    },
    {
      type: "category",
      label: "Spring Native项目接入",
      collapsed: true,
      items: [
        "non-springboot/non-springboot-dubbo-or-dubbox",
        "non-springboot/non-springboot-openfeign",
        "non-springboot/non-springboot-other",
      ],
    },
    {
      type: "doc",
      id: "q&a",
    },
  ],
};
