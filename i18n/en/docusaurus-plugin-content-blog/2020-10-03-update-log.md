---
slug: update-log
title: update record
author: 铂赛东
author_title: Architect & Open Source Author & Content Creator
author_image_url: /img/author.png
---

:::note v1.4.3

enhancement: #I5AKAL Reduce reliance on tripartite libraries like Fastjson and Hutool

https://gitee.com/dromara/TLog/issues/I5AKAL

:::

:::note v1.4.1

bug: #I57622 Fixed bug where log4j2 does not print custom tags in version 1.4.0

https://gitee.com/dromara/TLog/issues/I57622

:::

:::note v1.4.0

feature: #I49PSX Support for logstash

https://gitee.com/dromara/TLog/issues/I49PSX

enhancement: #I55M5H Optimized the MDC mode in logback

https://gitee.com/dromara/TLog/issues/I55M5H

enhancement: #I55M5J Optimizes asynchronous logs for log4j2

https://gitee.com/dromara/TLog/issues/I55M5J

:::

::: note v1.3.6

Feature: #I3UZNP TLog wishes to support RestTemplate

https://gitee.com/dromara/TLog/issues/I3UZNP

Enhancement: #I4LNF8 toggles the default ID generator to the mid-snowflake generator in the Hutool package

https://gitee.com/dromara/TLog/issues/I4LNF8

Bug: #I4LCD7 log4j2 In common asynchronous and MDC asynchronous scenarios, traceId is repeated

https://gitee.com/dromara/TLog/issues/I4LCD7

:::

:::note v1.3.5

Feature: #I4H26M Multi-project HTTP interactive print upstream and downstream project information support HuTool HttpUtil requests

https://gitee.com/dromara/TLog/issues/I4H26M

Feature: #I4L239 Tlog supports servlets

https://gitee.com/dromara/TLog/issues/I4L239

Feature: #I4L23L TLog supports forest framework call pass tags

https://gitee.com/dromara/TLog/issues/I4L23L

Bug: #I4HM98 Fixed TLog integration xxL-job with traceId

https://gitee.com/dromara/TLog/issues/I4HM98

Bug: #I4KIPK about SpanIdGenerator repeating spandId during asynchronous calls to generateNextSpanId

https://gitee.com/dromara/TLog/issues/I4KIPK

:::

:::note v1.3.4

Feature: #I4F9Y5 supports spring @Scheduled timers

https://gitee.com/dromara/TLog/issues/I4F9Y5

Enhancement: #I4FC3T Improves the low-level implementation of custom tags, using the expression engine

https://gitee.com/dromara/TLog/issues/I4FC3T

bug: #I4F4SL reprints labels when there are multiple %X values in logback MDC mode

https://gitee.com/dromara/TLog/issues/I4F4SL

:::

:::note v1.3.3

Feature: #I4EWIB TLogAspect value get value optimization, add constant value feature

https://gitee.com/dromara/TLog/issues/I4EWIB

Enhancement: #I4DJ0I adds support for HttpClint5

https://gitee.com/dromara/TLog/issues/I4DJ0I

Bug: #I4EQ6A Fixed custom convert missing one character

https://gitee.com/dromara/TLog/issues/I4EQ6A

#I4EQ6M Fixed bug where tlog-xxlJob could not be found in tomcat

https://gitee.com/dromara/TLog/issues/I4EQ6M

:::

:::note v1.3.2

enhancement: #I4D4XB Currently logback's asynchronous logs have excessive memory usage

https://gitee.com/dromara/TLog/issues/I4D4XB

enhancement: #I419TD Output logs, can print out current service ip

https://gitee.com/dromara/TLog/issues/I419TD

bug: #I3Y6LR The business tag cannot be displayed when the value type is long

https://gitee.com/dromara/TLog/issues/I3Y6LR

:::

:::note v1.3.1

bug: #I3RROF xxljob's dependencies will be forced to pass

https://gitee.com/dromara/TLog/issues/I3RROF

:::

:::note v1.3.0

feature: #I3OX16 Dependent module independence

https://gitee.com/dromara/TLog/issues/I3OX16

feature: #I3QVYE TLog's support for soul gateway

https://gitee.com/dromara/TLog/issues/I3QVYE

feature: #I3PVI1 Need to support the tag delivery of the mainstream cron framework

https://gitee.com/dromara/TLog/issues/I3PVI1

feature: #I3CILC Support xxl-job call link

https://gitee.com/dromara/TLog/issues/I3CILC

feature: #I1W7YA The framework level supports the call buried point of the client such as http client

https://gitee.com/dromara/TLog/issues/I1W7YA

enhancement: #I3PVG5 
At the gateway layer, TLogIdGenerator supports obtaining WebFlux exchange objects from extData

https://gitee.com/dromara/TLog/issues/I3PVG5

enhancement: #I3OUKW dubbo Conflict with the plug-in name of dubbox

https://gitee.com/dromara/TLog/issues/I3OUKW

enhancement: #I3QQ4Q In the process of solving the TLog pressure test, the performance is only about 60% without TLog

https://gitee.com/dromara/TLog/issues/I3QQ4Q

enhancement: #I39YHS Use nginx as the load, the request header log identification information cannot be transmitted

https://gitee.com/dromara/TLog/issues/I39YHS

bug: #I3IDND @TLogAspect annotation will clear the context

https://gitee.com/dromara/TLog/issues/I3IDND

bug: #I3PJ7G TLog The gateway implementation does not clear ThreadLocal

https://gitee.com/dromara/TLog/issues/I3PJ7G

:::

:::note v1.2.6

bug: #I3ON9Y TLog will report an error when the post interface accepts files to upload to the server

https://gitee.com/dromara/TLog/issues/I3ON9Y

:::

:::note v1.2.5

bug: #I3BYAN 
In normal spring mode, if TLogPropertyInit does not add TLogDefaultIdGenerator, an error will be reported

https://gitee.com/dromara/TLog/issues/I3BYAN

bug: #I3MLE0 In the logback synchronous log, the asynchronous thread log is printed in MDC mode, but the label cannot be printed

https://gitee.com/dromara/TLog/issues/I3MLE0

bug: #I3MLBE In log4j2 synchronous log, MDC mode uses asynchronous thread, and the label cannot be printed

https://gitee.com/dromara/TLog/issues/I3MLBE

bug: #I3KKP9 duplicate traceid

https://gitee.com/dromara/TLog/issues/I3KKP9

enhancement: #I3IS42 Support hostname local cache to avoid slow request due to improper server cooperation

https://gitee.com/dromara/TLog/issues/I3IS42

bug: #I3G94J The log4j2 log enhancement failed, the following warning was started, and tlog did not take effect

https://gitee.com/dromara/TLog/issues/I3G94J

enhancement: #I3F6BO TLog currently does not support Ali's ttl tool, hope to support

https://gitee.com/dromara/TLog/issues/I3F6BO

bug: #I3E3MZ In the asynchronous log, after the mdc is configured, the log content is printed repeatedly

https://gitee.com/dromara/TLog/issues/I3E3MZ

enhancement: #I3CUCJ Can you add a configuration that explicitly turns on the MDC mode

https://gitee.com/dromara/TLog/issues/I3CUCJ

enhancement: #I39MUX I hope Tlog supports HTTP get post put delete request parameters to be automatically printed to the log

https://gitee.com/dromara/TLog/issues/I39MUX

bug: #I2C989 The TLogMqWrapBean packaging enhancement class has no parameterless constructor, which causes failure when using json serialization

https://gitee.com/dromara/TLog/issues/I2C989

bug: #I2AZ6U The @Async label is used in the asynchronous log of log4j2, and the label cannot be printed

https://gitee.com/dromara/TLog/issues/I2AZ6U

enhancement: #I28R85 The run method in TLogInheritableTask will be rewritten

https://gitee.com/dromara/TLog/issues/I28R85

:::

:::note v1.2.4

feature: #I39IYV Support traceId to return to response header

https://gitee.com/dromara/TLog/issues/I39IYV

:::

:::note v1.2.4-BETA3

bug: #I394YM In MDC mode, the @TLogAspect tag does not take effect

https://gitee.com/dromara/TLog/issues/I394YM

:::

:::note v1.2.4-BETA2

bug: #I3933C Under Springboot 1.5.X, the web interceptor starts to report an error

https://gitee.com/dromara/TLog/issues/I3933C

:::

:::note v1.2.4-BETA1

enhancement: #I23U3Q Support spring cloud gateway in the spring cloud scenario

https://gitee.com/dromara/TLog/issues/I23U3Q

:::

:::note v1.2.3

bug: #I2D9L5 The newly added custom Id generator will generate bugs in non-springboot projects, resulting in the inability to generate traceids normally

https://gitee.com/dromara/TLog/issues/I2D9L5

:::

:::note v1.2.2

bug: #I2AZ6U The @Async label is used in the asynchronous log of log4j2, and the label cannot be printed

https://gitee.com/dromara/TLog/issues/I2AZ6U

:::

:::note v1.2.1

bug: #I29EQY Fix the bug that none of the static resources of the web project can be accessed in the v1.2.0 version

https://gitee.com/dromara/TLog/issues/I29EQY

feature: #I29FK8 Provide extensions for custom traceId generators

https://gitee.com/dromara/TLog/issues/I29FK8

:::

:::note v1.2.0

Changes refer to v1.2.0-BETA1 and v1.2.0-BETA2

:::

:::note v1.2.0-BETA2

bug: #I27TJJ dubbo 2.7.X version print call time does not display the error of the interface

https://gitee.com/dromara/TLog/issues/I27TJJ

:::

:::note v1.2.0-BETA1

bug: #I274EL Logback defines the problem of multiple appenders that may repeatedly display log labels

https://gitee.com/dromara/TLog/issues/I274EL

enhancement: #I2720D tlog currently needs to support log4j2 asynchronous logging

https://gitee.com/dromara/TLog/issues/I2720D

enhancement: #I260JE Refactor and encapsulate the rpc interception code

https://gitee.com/dromara/TLog/issues/I260JE

enhancement: #I260EP Completion of comments on code

https://gitee.com/dromara/TLog/issues/I260EP

enhancement: #I24YE8 Add the last label to call host

https://gitee.com/dromara/TLog/issues/I24YE8

enhancement: #I232BJ Analysis of json type by custom label annotation

https://gitee.com/dromara/TLog/issues/I232BJ

feature: #I22U5H Provide tracking support for message middleware

https://gitee.com/dromara/TLog/issues/I22U5H

feature: #I24YCX Provide call parameters for dubbo and feign, and automatically print call duration

https://gitee.com/dromara/TLog/issues/I24YCX

:::

:::note v1.1.7

bug: #I26WZY:Asynchronous logs in logback will have a bug that ignores formatting

https://gitee.com/dromara/TLog/issues/I26WZY

bug: #I26WZR:The MDC plugin sometimes fails under log4j2

https://gitee.com/dromara/TLog/issues/I26WZR

:::

:::note v1.1.5

bug: #I23XWU Under traditional spring+tomcat, the TLogWebConfig class does not work

https://gitee.com/dromara/TLog/issues/I23XWU

feature: #I230UI Support the MDC mode of slf4j, you can put the label in any position

https://gitee.com/dromara/TLog/issues/I230UI

:::

:::note v1.1.4

bug: #I22NLH Dubbo The attachments of Invocation of version 2.7.7 will be overwritten by the attachments of RpcContext

https://gitee.com/dromara/TLog/issues/I22NLH

:::

:::note v1.1.3

enhancement: #I1Z4Z9:Synchronous and asynchronous codes are integrated in the bytecode enhancement mode of logback

https://gitee.com/dromara/TLog/issues/I1Z4Z9

Bug: #I1YXKR:In the logback log framework, some logs do not have formatting parameters

https://gitee.com/dromara/TLog/issues/I1YXKR

Bug: #I1YVPJ:The synchronization log of logback in bytecode logic may not be intercepted in some cases, resulting in failure.

https://gitee.com/dromara/TLog/issues/I1YVPJ

:::

:::note v1.1.2

feature: #I1X29N Provide a javaagent method for the use of TLog, without intrusive access

https://gitee.com/dromara/TLog/issues/I1X29N

:::

:::note v1.1.1

enhancement: #I1XD4U supports the asynchronous log appender method in log4j

https://gitee.com/dromara/TLog/issues/I1XD4U

enhancement: #I1X35S Support AsyncAppender asynchronous log in logback

https://gitee.com/dromara/TLog/issues/I1X35S

enhancement: #I1XD5X Increase the asynchronous bytecode enhancer of each log frame

https://gitee.com/dromara/TLog/issues/I1XD5X

enhancement: #I1XBOL Bytecode enhancement The code here is not very readable and needs to be refactored

https://gitee.com/dromara/TLog/issues/I1XBOL

enhancement: #I1WV5H TLogContext method extension

https://gitee.com/dromara/TLog/issues/I1WV5H

enhancement: #I1WV55 If the custom configuration cannot be obtained, it should not display unknow

https://gitee.com/dromara/TLog/issues/I1WV55

:::

FEATURE: Add the spanId tag, spanId represents the position of this call in the entire call link tree

FEATURE: Annotation-based manual service label embedding feature

FIX: Springboot 1.5.X is supported

:::

:::note v1.0.3

FIX: #I1VW1O: In the thread pool scenario, the main thread will change, but the child thread will not change with the request.

https://gitee.com/dromara/TLog/issues/I1VW1O

:::

:::note v1.0.2

FIX: #I1VTLQ: The ThreadLocal object in TLogContext cannot be removed, which may lead to OOM

https://gitee.com/dromara/TLog/issues/I1VTLQ

FIX: #I1VSBV:LogWebInterceptor does not exclude static static resources, the traceId will be regenerated when static resources enter

https://gitee.com/dromara/TLog/issues/I1VSBV
:::



