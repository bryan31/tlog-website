---
slug: group-chat
title: Join group chat
author: 铂赛东
author_title: Architect & Open Source Author & Content Creator
author_image_url: /img/author.png
---

TLog is a new open source project, and the community is also very active. Since the open source, I have received feedback from many people and listened to these feedbacks for iterative and new feature development.

The future blueprint of TLog is to make a log field that integrates collection, tracking, statistics, analysis, and display in a log middleware. This requires your participation and feedback. You can join group chats, participate in discussions and feedback questions.

Since the community group has increased to more than 200 people, you can follow my WeChat official account, click on personal WeChat, and remark tlog when adding friends, and I will pull you into the group

![img](/img/offical-wx.jpg)

There are also QQ groups. Those who are used to QQ groups can add group questions and feedback

QQ group：882918944


