---
slug: about-author
title: About author
author: 铂赛东
author_title: Architect & Open Source Author & Content Creator
author_image_url: /img/author.png
---

I am an open source author and a content creator. Deeply cultivating the Java back-end for more than ten years, focusing on technical research and original sharing in the fields of architecture, open source, microservices, and distributed.

TLog is one of my most recent open source projects, personally maintained, and will always be maintained iteratively. If you like this project, please give me a star on Gitee and Github. It is not easy to open source personally. I hope to get your support and encouragement!

TLog's Gitee repository: https://gitee.com/dromara/TLog

TLog's Github repository: https://github.com/dromara/TLog

You can also follow my official account, I will share practical techniques, sort out knowledge, general weekly updates, and grow and progress with you on the road of technology.

![img](/img/offical-wx.jpg)
