---
id: spanid-rule
title: SpanId generation rules
sidebar_label: SpanId generation rules
---

The default label printing template of TLog is `<$spanId><$traceId>`

The SpanId in TLog represents the position of this call in the entire call link tree. Assuming that a Web system A receives a user request, then in the system log, the recorded SpanId is 0, which means it is the root of the entire call. Node, if system A processes this request and needs to call three systems B, C, and D in turn through RPC, then in the client log of system A, SpanId is 0.1, 0.2 and 0.3, respectively, in B, C, and D In the server log of a system, the SpanId is also 0.1, 0.2 and 0.3 respectively; if the C system calls the E and F systems when processing the request, then the corresponding client log in the C system is 0.2.1 and The server logs corresponding to the two systems 0.2.2, E and F are also 0.2.1 and 0.2.2. According to the above description, we can know that if all the SpanIds in a call are collected, a complete link tree can be formed.

We assume that the TraceId generated in a distributed call is `0a1234` (actually not so short), then according to the SpanId generation process above, there is the following figure:

![3](/img/spanid.png)
