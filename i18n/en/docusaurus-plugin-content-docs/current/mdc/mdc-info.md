---
id: mdc-info
title: The MDC mode description
sidebar_label: The MDC mode description
---

By default, T Log tags are placed before the body of a specific Log message, but if you want to customize the location, for example, before '[INFO]', is there any way?

T Log from 1.1.5, with the SLF 4J MDC adaptation, so that your label can be printed anywhere in a Log line. 'But only log frame configuration access' is supported. For JavaAgent and bytecode injection, customization at any location is not supported. For details about how to customize the log framework, see Log Framework Configuration.