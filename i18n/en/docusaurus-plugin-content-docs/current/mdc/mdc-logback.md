---
id: mdc-logback
title: Configure Logback in MDC mode
sidebar_label: Configure Logback in MDC mode
---

The MDC placeholder for logback in TLog is' %X{tl} '

Since V1.4.0, the MDC log configuration for Logback is different from 1.3.x. Much simpler, universal in synchronous & asynchronous mode.

If there is an old Logback configuration, change it to the following format; otherwise, labels may not be correctly displayed in V1.4.0.

Add an MDC Listener at the beginning, for example:


```xml {4}
<?xml version="1.0" encoding="UTF-8"?>
<configuration debug="false">
  	<!-- Add the following TLog MDC Listener -->
    <contextListener class="com.yomahub.tlog.core.enhance.logback.TLogLogbackTTLMdcListener"/>
    <!--Define the storage address for log files. Do not use a relative path in LogBack configuration-->
    <property name="LOG_HOME" value="./logs" />
    <!--Console log-->
    <appender name="Console" class="ch.qos.logback.core.ConsoleAppender">
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %X{tl} [%thread] %-5level %logger{50} - %msg%n</pattern>
        </encoder>
    </appender>

    <!--Log file-->
    <!--Synchronous log-->
    <appender name="SyncLogFile"  class="ch.qos.logback.core.rolling.RollingFileAppender">
        <File>${LOG_HOME}/logback-sync-rolling-mdc.log</File>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <!--Name of the output log file-->
            <FileNamePattern>${LOG_HOME}/logback-rolling-mdc.log.%d{yyyy-MM-dd}.%i.log</FileNamePattern>
            <!--Retention days of log files-->
            <MaxHistory>30</MaxHistory>
            <!--Log file size-->
            <maxFileSize>1000MB</maxFileSize>
        </rollingPolicy>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--Format output: %d indicates the date, %thread indicates the thread name, %-5 level: level displays 5 character widths from the left % MSG: log message, %n is a newline character-->
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %X{tl} [%thread] %-5level %logger{50} - %msg%n</pattern>
        </encoder>
    </appender>

    <!-- Log Output Level -->
    <root level="INFO">
        <appender-ref ref="Console" />
        <appender-ref ref="SyncLogFile" />
    </root>
</configuration>

```

