---
id: mdc-log4j
title: Log4j configuration in MDC mode
sidebar_label: Log4j configuration in MDC mode
---

The MDC placeholder for log4j in TLog is' %X{tl} '

You can specify both synchronous and asynchronous patterns in the pattern configuration file, as follows:

```xml {8,17}
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">
<log4j:configuration>
    <!--Console log-->
    <!--Replace this with AspectLog4jPatternLayout-->
    <appender name="Console" class="org.apache.log4j.ConsoleAppender">
        <layout class="com.yomahub.tlog.core.enhance.log4j.AspectLog4jPatternLayout">
            <param name="ConversionPattern" value="%d{yyyy-MM-dd HH:mm:ss,SSS} %X{tl} [%t] [%p] %m  >> %c:%L%n"/>
        </layout>
    </appender>

    <!--Log file-->
    <!--Synchronize logs: Just replace them with AspectLog4jPatternLayout-->
    <appender name="SyncFile" class="org.apache.log4j.DailyRollingFileAppender">
        <param name="File" value="./logs/log4j-sync-mdc.log"/>
        <layout class="com.yomahub.tlog.core.enhance.log4j.AspectLog4jPatternLayout">
            <param name="ConversionPattern" value="%d{yyyy-MM-dd HH:mm:ss,SSS} %X{tl} [%t] [%p] %m  >> %c:%L%n"/>
        </layout>
    </appender>

    <root>
        <priority value="info" />
        <appender-ref ref="Console"/>
        <appender-ref ref="SyncFile"/>
    </root>
</log4j:configuration>
```

