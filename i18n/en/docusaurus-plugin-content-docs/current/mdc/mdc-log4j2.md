---
id: mdc-log4j2
title: Configure Log4j2 in MDC mode
sidebar_label: Configure Log4j2 in MDC mode
---

The placeholder for TLog in log4j2 is' %TX{tl} ', which is different from everything in log4j and logback.

The following example also applies to synchronous and asynchronous modes:

```xml {8,17}
<?xml version="1.0" encoding="UTF-8"?>
<configuration status="WARN" monitorInterval="30">
    <!--Define all appenders first-->
    <appenders>
        <!--Console log-->
        <console name="Console" target="SYSTEM_OUT">
            <!--Format of the output log-->
            <PatternLayout pattern="%date{HH:mm:ss.SSS} %TX{tl} %-5level [%thread] %logger{36} - %tmsg%n"/>
        </console>

        <!--Log file-->
        <!--Synchronous log-->
        <RollingFile name="RollingFileInfo" fileName="./logs/log4j2-sync-rolling-mdc.log"
                     filePattern="${sys:user.home}/logs/$${date:yyyy-MM}/info-%d{yyyy-MM-dd}-%i.log">
            <!--The console outputs only messages of level and above (onMatch), others are rejected (onMismatch).-->
            <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            <PatternLayout pattern="%date{HH:mm:ss.SSS} %TX{tl} %-5level [%thread] %logger{36} - %tmsg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="100 MB"/>
            </Policies>
        </RollingFile>

        <!--Synchronous log-->
        <File name="SyncFile" fileName="./logs/log4j2-sync-mdc.log" append="false" >
            <PatternLayout pattern="%date{HH:mm:ss.SSS} %TX{tl} %-5level [%thread] %logger{36} - %tmsg%n"/>
        </File>

    </appenders>
    <!--Logger is then defined. An appender will not take effect until logger is defined and an appender is introduced-->
    <loggers>
        <root level="INFO">
            <appender-ref ref="Console"/>
            <appender-ref ref="SyncFile"/>
            <appender-ref ref="RollingFileInfo"/>
        </root>
    </loggers>
</configuration>
```