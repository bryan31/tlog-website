---
id: features
title: Project characteristics
sidebar_label: Project characteristics
---

The currently supported features of TLog are as follows:

-Complete lightweight microservice log tracking by tagging logs
-Provides three access methods: javaagent is completely non-intrusive access, bytecode one line of code access, and configuration file-based access
-Non-intrusive design for business code, easy to use, access in 10 minutes
-Support the common log4j, log4j2, logback three log frameworks, and provide automatic detection to complete the adaptation
-Support dubbo, dubbox, springcloud three RPC frameworks
-Support Spring Cloud Gateway and Soul Gateway
-Adapt to HttpClient and Okhttp's http call tag delivery
-Support three task frameworks, TimerTask of JDK, Quartz, XXL-JOB
-Support the configuration of the custom template of the log label, and provide multiple system-level buried point label options
-Supports tracking of asynchronous threads, including scenarios such as thread pools, multi-level asynchronous threads, etc.
-Almost no performance loss, fast and stable, after pressure test, the loss is 0.01%
