---
id: javaagent-type-shell-config
title: Java startup parameter configuration
sidebar_label: Java startup parameter configuration
---

To use the javaagent method, you only need to add in your java startup parameters:
```shell script
-javaagent:/your_path/tlog-agent.jar
```

The latest tlog-agent.jar can be downloaded at the following address

https://gitee.com/dromara/TLog/releases/v1.4.3
