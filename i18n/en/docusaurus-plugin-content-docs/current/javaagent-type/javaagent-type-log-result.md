---
id: javaagent-type-log-result
title: Final log effect
sidebar_label: Final log effect
---

Only need this step, you can quickly access the springboot project

Here take dubbo+log4j as an example, the Consumer code

![1](/img/log_result_1.png)

Log printing:

```
2020-09-16 18:12:56,748 [WARN] [TLOG]重新生成traceId[7161457983341056]  >> com.yomahub.tlog.web.TLogWebInterceptor:39
2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> logback-dubbox-consumer:invoke method sayHello,name=jack  >> com.yomahub.tlog.example.dubbox.controller.DemoController:22
2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> 测试日志aaaa  >> com.yomahub.tlog.example.dubbox.controller.DemoController:23
2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> 测试日志bbbb  >> com.yomahub.tlog.example.dubbox.controller.DemoController:24
```



Provider code:

![2](/img/log_result_2.png)



Log printing:

```
2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> logback-dubbox-provider:invoke method sayHello,name=jack  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:15
2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> 测试日志cccc  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:16
2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> 测试日志dddd  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:17
```



It can be seen that after simple access, each request between each microservice has a globally unique traceId running through it, which is valid for all log outputs, and it becomes easy to locate the log chain of a request. .


:::note
The 0 and 0.1 in front of traceId are spanId. For the description of spanId, please refer to `9. SpanId generation rules`.
:::
