---
id: javaagent-type-info
title: Javaagent access method
sidebar_label: Javaagent access method
---

:::info
This method does not invade the project at all. Using Javaagent to add the jar package at startup, the whole process can be done in 1 minute.
No matter if your RPC is dubbo/dubbox or spring cloud, no matter if the logging framework you use is log4j, log4j2 or logback, tlog-agent can automatically adapt
However, this method requires your project to be a springboot project. If your project is not a springboot project, please refer to `Log Framework Adaptation Method`
:::

:::caution
Although this method does not invade the project at all, it also provides the least feature support. MDC and asynchronous logging are not supported.
:::