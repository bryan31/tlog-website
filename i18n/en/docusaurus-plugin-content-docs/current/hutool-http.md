---
id: hutool-http
title: Support for hutool-http
sidebar_label: support for hutool-htpp
---

TLog has support for HTTP modules in the hutool framework since 1.3.5. Now you can also easily pass tag information using the hutool-http module.

:::info
Note that hutool only supports `interceptor` settings from `5.7.16`, so make sure your hutool is only provided in '5.7.16+'
:::

You need to set the `interceptor` in the hutool-http call

```java {6}
// set the TLogHutoolhttpInterceptor into instance attributes, best every time don't go to the new
TLogHutoolhttpInterceptor tLogHutoolhttpInterceptor = new TLogHutoolhttpInterceptor();

String result2 = HttpRequest.post(url)
.header(header. USER_AGENT, "Hutool HTTP")// Header information
.addInterceptor(tLogHutoolhttpInterceptor)
.form(paramMap)// Form content
.timeout(20000)// Timeout, ms
.execute().body();
```