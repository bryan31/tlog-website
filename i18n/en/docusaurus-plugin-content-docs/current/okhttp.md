---
id: okhttp
title: Support for Okhttp
sidebar_label: Support for Okhttp
---

At present, TLog's support for Okhttp client is intrusive. For the requester, an interceptor needs to be added to pass the log tag information record to the server using the Http Header.

Add interceptor `TLogOkHttpInterceptor`

```java
     String url = "http://127.0.0.1:2111/hi?name=2323";
     //Create OkHttpClient request object
     OkHttpClient client = new OkHttpClient().newBuilder()
             .addInterceptor(new TLogOkHttpInterceptor())
             .build();
     //Create Request
     Request request = new Request.Builder().url(url).build();
     //Get the Call object
     Call call = client.newCall(request);
     try {
         Response response = call.execute();
         log.info("http response result:{}",response.body().string());
     } catch (IOException e) {
         e.printStackTrace();
     }
```