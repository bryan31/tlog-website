---
id: custom_traceid
title: Custom TraceId generator
sidebar_label: Custom TraceId generator
---

TLog uses snowflake algorithm to generate traceId by default. Of course, you can also change the traceId generation algorithm.

Define your own traceId generation class to inherit the `TLogIdGenerator` interface:

```java
public class TestIdGenerator extends TLogIdGenerator {
    @Override
    public String generateTraceId() {
        return String.valueOf(System.nanoTime());
    }
}
```

Then declare in the configuration class of springboot:

```properties
tlog.id-generator=com.yomahub.tlog.example.dubbo.id.TestIdGenerator
```