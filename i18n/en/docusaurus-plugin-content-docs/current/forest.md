---
id: forest
title: Support for forest
sidebar_label: Support for forest
---

TLog also has support for the HTTP framework Forest since 1.3.5.

You need to configure the `TLogForestInterceptor` interceptor wherever it is needed. Here's an example of a single method:

```java {5}
public interface SimpleClient {
@Request(
url = "http://localhost:8080/hello/user? username=foo",
headers = {"Accept:text/plain"},
interceptor = TLogForestInterceptor.class
)
String simple();
}
```

Forest has a variety of configurations for interceptors and supports multiple interceptor configurations. For other usage methods, please refer to the Forest documentation:

https://forest.dtflyx.com/docs/adv/interceptor