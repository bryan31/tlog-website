---
id: getting-started
title: What pain points can tlog solve
sidebar_label: What pain points can TLog solve
slug: /
---

With the prevalence of microservices, many companies have dismantled the system into many microservices according to business boundaries, and when troubleshooting and checking logs. Because the business link runs through many microservice nodes, it becomes difficult to locate the log of a certain request and the log of upstream and downstream services.

At this time, many people will start to consider SkyWalking, Pinpoint and other distributed tracking systems to solve them. They are based on the OpenTracing specification, and are usually non-invasive, and have a relatively friendly management interface for link Span queries.

However, to build a distributed tracking system, it takes a certain period of time to be familiar with and promote the system to the whole company, and it involves the storage cost of link span nodes, full collection or partial collection? If the full amount is collected, take SkyWalking's storage as an example. At least 5 nodes are required to build an ES cluster. This requires an increase in server costs. Moreover, if there are many microservice nodes, it is normal to generate tens of gigabytes or hundreds of gigabytes of data in one day. If you want to keep it for a longer time, you also need to increase the cost of the server disk.

Of course, the distributed tracing system is the ultimate solution. If your company is already on the distributed tracing system, TLog is not applicable.

:::info
TLog provides the easiest way to solve the log tracking problem. It does not collect logs and does not require additional storage space. It just automatically tags your logs and automatically generates TraceId throughout your microservices. Links. And provide upstream and downstream node information. Suitable for small and medium-sized enterprises and company projects that want to quickly solve log tracking problems.
:::

For this reason, TLog adapts to three log frameworks and supports automatic detection and adaptation. It supports the three RPC frameworks of dubbo, dubbox, and spring cloud. More importantly, you may not need to connect your project to TLog for ten minutes :)