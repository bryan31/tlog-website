---
id: bytes-type-info
title: Bytecode injection method
sidebar_label: Bytecode injection method
---

:::info
This method is suitable for springboot projects, which need to depend on the tlog-all-spring-boot-starter package,
tlog provides the automatic assembly function of springboot. If your project is not a springboot project, please refer to `Log Framework Adaptation Method`
:::

:::caution
This method is similar to javaagent access. Although simple, it does not support MDC and asynchronous logging. And only supports springboot projects.
:::