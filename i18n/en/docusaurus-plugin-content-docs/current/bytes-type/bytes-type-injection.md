---
id: bytes-type-injection
title: Specific method of bytecode injection
sidebar_label: Specific method of bytecode injection
---

Just add a line of code to your startup class, you can automatically detect and enhance the Log framework used by your project.

The following methods are suitable for log4j and logback. In principle, log4j2 does not even need this line, because log4j2 will plug-in architecture design and will automatically read the log4j2 adapter plug-ins in the TLog project.

The following methods also support the asynchronous log format of the three log frameworks, please rest assured to use

```java {4}
@SpringBootApplication
public class Runner {

    static {AspectLogEnhance.enhance();}//Perform log enhancement and automatically determine the log framework

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }
}
```

:::caution
Because this is implemented with javassist, it is necessary to perform bytecode enhancement before the JVM loads the classes of the corresponding log framework. So the static block is used here. But this method should pay attention to the following points:

-For Springboot applications, the log definition cannot be added to the startup class, otherwise it will not take effect.

-If your project is non-springboot, or you start it with an external container such as tomcat/jboss/jetty (spring-boot-starter-web of springboot is a built-in container), then this method cannot be used and you can only manually modify the log configuration File, it is very convenient to replace several classes, please refer to `Log Framework Adaptation Method`

-For applications using the log4j2 logging framework, if this method does not work, please change the `m/msg/message` in the `pattern` of the log4j2 configuration file to `tm/tmsg/tmessage`
:::


