---
id: logstash-logback
title: Logback and Logstash integration
sidebar_label: Logback and Logstash integration
---

Starting from V1.4.0, TLog adds the adaptation of the LogStash socket layer to the Logback, so that TLog can directly send socket data to the Logstash server.

The MDC supports both synchronous and asynchronous.

:::info

Currently, only the Logback framework is supported. The log4j and log4j 2 frameworks are not currently supported

:::



If you want to integrate logstash, first add a dependency to the POM:

```xml
<dependency>
  <groupId>com.yomahub</groupId>
  <artifactId>tlog-logstash-logback</artifactId>
  <version>1.4.3</version>
</dependency>
```



Then you need to add an appender to your Logback configuration file, for example:

```xml
<!-- Json-formatted Appender for logstash output -->
<appender name="logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
  <destination>Logstash服务端IP:端口</destination>
  <keepAliveDuration>5 minutes</keepAliveDuration>
  <!-- 日志输出编码 -->
  <encoder charset="UTF-8" class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
    <providers>
      <provider class="com.yomahub.tlog.logstash.logback.TLogLogstashLogbackProvider"/>
      <pattern>
        <pattern>
          {
          "level": "%level",
          "thread": "%thread",
          "class": "%logger{40}",
          "message": "%message",
          "stack_trace": "%exception{10}",
          "client_time": "%d{yyyy-MM-dd HH:mm:ss.SSS}"
          }
        </pattern>
      </pattern>
    </providers>
  </encoder>
</appender>
```



And that's it. Once it's started, when it outputs the log, it outputs the label information and the log information to the logstash.



The logstash console output can be used to test the logstash configuration file as follows:

```
input {
   tcp {
      port => 9800
   }
}
filter {
   json {
      source => "message"
   }
}
output {
   stdout { codec => rubydebug }
}
```

