---
id: httpclient
title: Support for Httpclient
sidebar_label: Support for Httpclient
---

At present, TLog's support for Httpclient client is intrusive, and an interceptor needs to be added to the requester to pass the log tag information record to the server using the Http Header.

HttpClient 4 X add interceptors ` TLogHttpClientInterceptor `

Code example:

```java
     String url = "http://127.0.0.1:2111/hi?name=2323";
     CloseableHttpClient client = HttpClientBuilder.create()
                 .addInterceptorFirst(new TLogHttpClientInterceptor())
                 .build();
     HttpGet get = new HttpGet(url);
     try {
         CloseableHttpResponse response = client.execute(get);
         HttpEntity entity = response.getEntity();
         log.info("http response code:{}", response.getStatusLine().getStatusCode());
         log.info("http response result:{}",entity == null? "": EntityUtils.toString(entity));
     } catch (IOException e) {
         e.printStackTrace();
     }
```

HttpClient 5. X add interceptors ` TLogHttpClient5Interceptor `

HttpClient 5.X 添加拦截器`TLogHttpClient5Interceptor`

```java
    String url = "http://127.0.0.1:2111/hi?name=2323";
    CloseableHttpClient client = HttpClientBuilder.create()
                .addInterceptorFirst(new TLogHttpClient5Interceptor())
                .build();
    HttpGet get = new HttpGet(url);
    try {
        CloseableHttpResponse response = client.execute(get);
        HttpEntity entity  = response.getEntity();
        log.info("http response code:{}", response.getStatusLine().getStatusCode());
        log.info("http response result:{}",entity == null ? "" : EntityUtils.toString(entity));
    } catch (IOException e) {
        e.printStackTrace();
    }
```