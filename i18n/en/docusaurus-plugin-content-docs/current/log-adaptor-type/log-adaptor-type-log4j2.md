---
id: log-adaptor-type-log4j2
title: Log4j2 framework adapter
sidebar_label: Log4j2 framework adapter
---

:::caution
Since log4j2 is a plugin mechanism, please change `m/msg/message` in `pattern` of log4j2 configuration file to 'tm/tmsg/tmessage' to avoid loading log4j2 into your plugin

If you don't want to change this, another option is to put the tlog dependency package in the POM file before the log4j2 dependency package.
:::

## Synchronous log

:::info
Because log4J2 is implemented in plug-in mode, log4J2 can automatically scan plug-ins. So it takes effect without making any changes to the configuration file in the case of log synchronization.
:::

The following is just an example of log4j2 synchronizing the log configuration file.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration status="WARN" monitorInterval="30">
    <!--先定义所有的appender-->
    <appenders>
        <!--控制台日志-->
        <console name="Console" target="SYSTEM_OUT">
            <!--输出日志的格式-->
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
        </console>

        <!--文件日志-->
        <!--同步日志-->
        <RollingFile name="RollingFileInfo" fileName="./logs/log4j2-sync-rolling.log"
                     filePattern="${sys:user.home}/logs/$${date:yyyy-MM}/info-%d{yyyy-MM-dd}-%i.log">
            <!--控制台只输出level及以上级别的信息（onMatch），其他的直接拒绝（onMismatch）-->
            <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="100 MB"/>
            </Policies>
        </RollingFile>

        <!--同步日志-->
        <File name="SyncFile" fileName="./logs/log4j2-sync.log" append="false" >
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
        </File>

    </appenders>
    <!--然后定义logger，只有定义了logger并引入的appender，appender才会生效-->
    <loggers>
        <root level="INFO">
            <appender-ref ref="Console"/>
            <appender-ref ref="SyncFile"/>
            <appender-ref ref="RollingFileInfo"/>
        </root>
    </loggers>
</configuration>
```

## Asynchronous log

:::info
Because log4J2 is implemented in plug-in mode, log4J2 can automatically scan plug-ins. It also takes effect without making any changes to the configuration file in the case of asynchronous logging.

In asynchronous logs, since log4j2 is sent to a queue, there is no relationship between the logging thread and the business thread. In order for log4j2 to get the label value from the business thread in any case, you need to configure the following code in the startup item:
:::

```java {2}
public static void main(String[] args) {
    System.setProperty("log4j2.isThreadContextMapInheritable", Boolean.TRUE.toString());
    SpringApplication.run(RunnerApplication.class, args);
}
```

If you are not a Springboot project and cannot easily set this parameter in the boot code, this parameter can also be set in the JVM parameters:

```shell
-Dlog4j2.isThreadContextMapInheritable=true
```

The following is just an example of a log4j2 asynchronous log configuration file that doesn't need to be changed:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration status="WARN" monitorInterval="30">
    <!--先定义所有的appender-->
    <appenders>
        <!--控制台日志-->
        <console name="Console" target="SYSTEM_OUT">
            <!--输出日志的格式-->
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
        </console>

        <!--文件日志-->
        <RollingFile name="RollingFileInfo" fileName="./logs/log4j2-async-rolling.log"
                     filePattern="${sys:user.home}/logs/$${date:yyyy-MM}/info-%d{yyyy-MM-dd}-%i.log">
            <!--控制台只输出level及以上级别的信息（onMatch），其他的直接拒绝（onMismatch）-->
            <ThresholdFilter level="info" onMatch="ACCEPT" onMismatch="DENY"/>
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy/>
                <SizeBasedTriggeringPolicy size="100 MB"/>
            </Policies>
        </RollingFile>

        <!--异步日志：一 先声明同步文件-->
        <File name="AsyncLogFile" fileName="./logs/log4j2-async.log" append="false" >
            <PatternLayout pattern="%date{HH:mm:ss.SSS}  %-5level [%thread] %logger{36} - %tmsg%n"/>
        </File>
        <!--异步日志：二 使用Async配置异步 -->
        <Async name="AsyncFile">
            <AppenderRef ref="AsyncLogFile"/>
        </Async>

        <!--异步输出到控制台-->
        <Async name="AsyncConsole">
            <AppenderRef ref="Console"/>
        </Async>

        <!--异步输出到滚动日志-->
        <Async name="AsyncRolling">
            <AppenderRef ref="RollingFileInfo"/>
        </Async>

    </appenders>
    <!--然后定义logger，只有定义了logger并引入的appender，appender才会生效-->
    <loggers>
        <root level="INFO">
            <appender-ref ref="AsyncFile"/>
            <appender-ref ref="AsyncConsole"/>
            <appender-ref ref="AsyncRolling"/>
        </root>
    </loggers>
</configuration>
```