---
id: log-adaptor-type-info
title: Log frame adapter method
sidebar_label: Log frame adapter method
---

:::info
TLog provides a way to adapt to each log framework. You need to modify the configuration file of the log and replace the corresponding class. The configuration method is also very simple. An example of each scenario is given below.

**This method supports all features and is the official recommended method**
:::