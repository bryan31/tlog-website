---
id: invoke-time
title: Automatically print call parameters and time
sidebar_label: Automatically print call parameters and time
---



For springboot applications, you only need to configure the following in the springboot configuration file:

```properties
# If not equipped, the default is false
tlog.enable-invoke-time-print=true
```



When you make an RPC call, the parameter information and call time information will be automatically printed out:

```
2020-12-01 19:20:07.768 [DubboServerHandler-127.0.0.1:30900-thread-2] INFO cytlog.dubbo.filter.TLogDubboInvokeTimeFilter-<0.1><7592057736843136> [TLOG] Start calling the method of interface [DemoService] [sayHello], the parameter is: ["jack"]
2020-12-01 19:20:07.787 [DubboServerHandler-127.0.0.1:30900-thread-2] INFO cytexample.dubbo.service.impl.DemoServiceImpl-<0.1><7592057736843136> logback-dubbox-provider:invoke method sayHello ,name=jack
2020-12-01 19:20:07.788 [Thread-14] INFO c.y.tlog.example.dubbo.service.impl.AsynDomain-<0.1><7592057736843136> This is an asynchronous method
2020-12-01 19:20:07.788 [Thread-14] INFO c.y.tlog.example.dubbo.service.impl.AsynDomain-<0.1><7592057736843136> Asynchronous method start
2020-12-01 19:20:07.789 [Thread-14] INFO c.y.tlog.example.dubbo.service.impl.AsynDomain-<0.1><7592057736843136> End of asynchronous method
2020-12-01 19:20:07.795 [DubboServerHandler-127.0.0.1:30900-thread-2] INFO cytlog.dubbo.filter.TLogDubboInvokeTimeFilter-<0.1><7592057736843136> [TLOG]End interface [DemoService] method[ sayHello] call, time-consuming: 90 milliseconds
```

