---
id: async-thread-common
title: General asynchronous thread
sidebar_label: General asynchronous thread
---

:::info
For general asynchronous threads (*), you don't need to do anything. TLog naturally supports printing labels in asynchronous threads.

*The definition of a general asynchronous thread is: the thread will be automatically destroyed after execution, such as the internal thread of new Thread(){...}.start, your custom-defined class inherits Thread or implements the Runnerable interface.
:::
