---
id: async-thread-pool
title: Thread Pool
sidebar_label: Thread Pool
---

But for the scenario where the thread pool is used, since the threads in the thread pool will not be destroyed, they will be reused. You need to replace `Runnable` with `TLogInheritableTask`, otherwise the tag data will be repeated:

```java {2}
ExecutorService pool = Executors.newFixedThreadPool(5);
pool.submit(new TLogInheritableTask() {
     @Override
     public void runTask() {
       log.info("I am an asynchronous thread log");
     }
});
```

In this way, the label data can be printed correctly.

In addition to using the enhanced classes provided above, here is also recommended an Ali's ThreadLocal framework, which can also perfectly solve the label delivery situation in the TLog thread pool scenario

https://github.com/alibaba/transmittable-thread-local

TLog also supports this framework. For how to use it, please go to the above address to check it yourself. The instructions and use cases have been written very clearly.