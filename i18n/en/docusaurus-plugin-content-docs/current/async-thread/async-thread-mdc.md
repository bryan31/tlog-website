---
id: async-thread-mdc
title: Asynchronous threads in MDC mode
sidebar_label: Asynchronous threads in MDC mode
---

If you use the label position customization function (this function is implemented with slf4j's MDC), then you may find that asynchronous threads in MDC mode seem to be unable to print labels normally. This is because `ThreadLocal` is used in MDC, and the information in `ThreadLocal` cannot be passed to child threads. For this reason, TLog provides a thread wrapper class, which can help you solve this problem.



Still the same class `TLogInheritableTask`, if you declare a thread, you need to inherit `TLogInheritableTask`:

```java
public class AsynDomain extends TLogInheritableTask {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void runTask() {
        log.info("This is an asynchronous method");
        log.info("Asynchronous method start");
        log.info("End of Asynchronous Method");
    }
}
```



The execution is the same as the normal inheritance of `Thread`:

```java
new AsynDomain().start();
```



If you want to execute in a thread pool, also taking `AsynDomain` as an example, it is:

```java
ExecutorService pool = Executors.newFixedThreadPool(5);
pool.submit(new AsynDomain());
```



