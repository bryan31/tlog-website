---
id: resttemplate
title: Support for RestTemplate
sidebar_label: Support for RestTemplate
---

TLog starts with 1.3.6, and RestTemplate is also supported in Spring Web.

You need to put the `TLogRestTemplateInterceptor` to the relevant areas are available, such as:

```java
restTemplate.setInterceptors(Collections.singletonList(new TLogRestTemplateInterceptor()));
```
