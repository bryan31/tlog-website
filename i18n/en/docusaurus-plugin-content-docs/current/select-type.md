---
id: select-type
title: How to choose your access method
sidebar_label: How to choose your access method
---

:::info
TLog provides three access methods, which are compatible with various project environments. Please refer to the following table to select the access method that suits your project.
:::

| | springboot project self-starting | non-springboot self-starting | springboot project external container | non-springboot project external container |
| ------------------ | ------------------------ | ----- ----------------- | ---------------------- | --------- --------------- |
| Javaagent method | <font color="ffce73">**suitable**</font> | not suitable | not suitable | not suitable |
| Bytecode injection method | <font color="ffce73">**suitable**</font> | <font color="ffce73">**suitable**</font> | not suitable | not suitable |
| Log frame adapter method (most stable) | <font color="ffce73">**Fit**</font> | <font color="ffce73">**Fit**</font> | <font color= "ffce73">**Fit**</font> | <font color="ffce73">**Fit**</font> |

**Self-starting refers to the main function as the project's startup entry (starter-web of the springboot project also belongs to the self-starting mode)**

**External container refers to the project deployed in a container similar to tomcat, tomcat is used as an external container, and the project is deployed in the webapp directory**

**Javaagent is essentially a bytecode injection method, but it is a completely non-intrusive project. Bytecode may fail in some complex projects due to different class loading mechanisms. Therefore, if your project structure is very complicated and you find that javaagent and bytecode methods do not work, then the log framework adapter method is recommended. , This kind of relatively most stable**



:::info
The TLog access method supports the features as shown in the table below
:::

| | Synchronous log | MDC | Asynchronous log |
| -------------------------- | ---------------------- -------------- | ----------------------------------- -| ------------------------------------ |
| Javaagent method | <font color="ffce73">**Support**</font> | Not Supported | Not Supported |
| Bytecode injection method | <font color="ffce73">**Support**</font> | Not supported | Not supported |
| Log frame adapter method (most stable) | <font color="ffce73">**Support**</font> | <font color="ffce73">**Support**</font> | <font color= "ffce73">**Support**</font> |

