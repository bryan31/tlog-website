---
id: dependency-ondemand
title: On-demand dependence
sidebar_label: On-demand dependence
---

If you don't want to rely on unnecessary packages, TLog provides on-demand dependencies for springboot

The template form is:

```xml
<dependency>
  <groupId>com.yomahub</groupId>
  <artifactId>tlog-XXX-spring-boot-starter</artifactId>
  <version>1.4.3</version>
</dependency>
```



The specific modules and descriptions are as follows

| Module name | Description |
| ------------------------------------------------- ----------- | ------------------------------------- |
| <font color="ffce73">**tlog-dubbo-spring-boot-starter**</font> | Projects for apache dubbo |
| <font color="ffce73">**tlog-dubbox-spring-boot-starter**</font> | Applicable to Dangdang's dubbox project |
| <font color="ffce73">**tlog-feign-spring-boot-starter**</font> | Applicable to open feign projects in spring cloud |
| <font color="ffce73">**tlog-gateway-spring-boot-starter**</font> | Applicable to gateway service in spring cloud |
| <font color="ffce73">**tlog-soul-spring-boot-starter**</font> | Suitable for soul gateway service |
| <font color="ffce73">**tlog-web-spring-boot-starter**</font> | Suitable for projects with spring web |
| <font color="ffce73">**tlog-xxljob-spring-boot-starter**</font> | Projects suitable for xxl-job |


:::info
All modules include log4j, log4j2, logback three log framework support, these modules can also be used in combination, for example, spring cloud openfeign requires tlog-feign-spring-boot-starter and tlog-web-spring-boot-starter at the same time
:::