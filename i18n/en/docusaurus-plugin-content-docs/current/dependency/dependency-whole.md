---
id: dependency-whole
title: Full dependence
sidebar_label: Full dependence
---

TLog provides two different dependencies on springboot and spring native. In this way, you only need to rely on one package, and the necessary packages will pass in the dependencies.

**springboot dependency**

```xml
<dependency>
   <groupId>com.yomahub</groupId>
   <artifactId>tlog-all-spring-boot-starter</artifactId>
   <version>1.4.3</version>
</dependency>
```

**spring native dependency**
```xml
<dependency>
   <groupId>com.yomahub</groupId>
   <artifactId>tlog-all</artifactId>
   <version>1.4.3</version>
</dependency>
```