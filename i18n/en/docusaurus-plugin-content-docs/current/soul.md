---
id: soul
title: Support for Soul Gateway
sidebar_label: Support for Soul Gateway
---

Starting from 1.3.0, TLog also supports the open source framework Soul Gateway

You only need to introduce the `tlog-web-spring-boot-starter` and `tlog-soul-spring-boot-starter` modules in the server project of the gateway.

TLog will automatically load and adapt when the gateway is started. For details on how to rely on it, please refer to `Install TLog / On-demand dependence`