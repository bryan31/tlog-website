---
id: q&a
title: Q & A
sidebar_label: Q & A
---

**<font color="ffce73">Q: Why is my request coming in from the controller, but the log label is not printed? </font>**

**A: If you define a web interceptor in your project, check whether it inherits the interface `WebMvcConfigurationSupport`. The interceptor of TLog implements the `WebMvcConfigurer` interface. In spring mvc, `WebMvcConfigurationSupport` and `WebMvcConfigurer `There will be conflicts. **

**The solution is: replace the inherited `WebMvcConfigurationSupport` with the implementation of the `WebMvcConfigurer` interface, multiple `WebMvcConfigurer` will not conflict **

**<font color="ffce73">Q：I use javaagent/bytecode access, why doesn’t it work?</font>**

**A: The javaagent method and the bytecode method are essentially the same in TLog, except that javaagent does not need to be dependent, and the bytecode is changed through javassit before the class is loaded. However, due to the large differences in the system of different companies, if there is a class that loads the relevant log framework in advance before TLog, the mark will be invalid**

**The solution is: change to the `log framework adapter method` to access, which is currently the most stable method and supports the most features**

**<font color="ffce73">Q: I use log4j2, why a lot of warning messages appear at the beginning of the log? Is this normal? </font>**

**A: This is normal, because the loading mechanism of log4j2 is in the form of a plug-in, and TLog needs to override the default plug-in of log4j2, so log4j2 will have a warning at startup to inform that the placeholder of mdc has been replaced**