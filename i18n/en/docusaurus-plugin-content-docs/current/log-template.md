---
id: log-template
title: Log label template customization
sidebar_label: Log label template customization
---

TLog only prints spanId and traceId by default, using the template `<$spanId><$traceId>`. Of course, you can customize the template. Other label headers can also be added

You only need to define the following in springboot application.properties:

```properties
tlog.pattern=[$preApp][$preIp][$spanId][$traceId]
```

`$preApp`: upstream microservice node name

`$preHost`: Host Name of the upstream microservice

`$preIp`: the IP address of the upstream microservice

`$spanId`: link spanId, for specific rules, please refer to `8. SpanId generation rules`

`$traceId`: Globally unique trace ID

In this way, the printing of the log can be printed according to the template you define
