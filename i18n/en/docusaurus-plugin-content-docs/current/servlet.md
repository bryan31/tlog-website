---
id: servlet
title: Support for servlets
sidebar_label: Support for servlets
---

While I wonder if anyone still uses servlet technology, TLog has also supported servlets since 1.3.5

You need to declare a filter and filter-mapping in `web.xml`

```xml {3}
<filter>
    <filter-name>tlogServletFilter</filter-name>
    <filter-class>com.yomahub.tlog.web.filter.TLogServletFilter</filter-class>
</filter>

<filter-mapping>
<filter-name>tlogServletFilter</filter-name>
<url-pattern>*.do</url-pattern>
</filter-mapping>
```