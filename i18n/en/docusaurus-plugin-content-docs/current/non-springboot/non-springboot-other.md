---
id: non-springboot-other
title: Other configuration
sidebar_label: Other configuration
---

If you want to customize the template display and turn on the rpc time printing display, you need to configure the following in spring xml

```xml
<bean class="com.yomahub.tlog.spring.TLogPropertyInit">
    <property name="pattern" value="[$preApp][$preHost][$preIp][$spanId][$traceId]"/>
    <property name="enableInvokeTimePrint" value="true"/>
</bean>
```