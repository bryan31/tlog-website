---
id: non-springboot-dubbo-or-dubbox
title: dubbo & dubbox
sidebar_label: dubbo & dubbox
---

If your RPC is dubbo or dubbox, you need to configure the following in spring xml

```xml
<bean class="com.yomahub.tlog.web.TLogWebConfig"/>
<bean class="com.yomahub.tlog.core.aop.AspectLogAop"/>
```

At the same time, the following configuration needs to be done in the spring mvc xml
```xml
<mvc:interceptors>
    <bean class="com.yomahub.tlog.web.interceptor.TLogWebInterceptor" />
</mvc:interceptors>
```
