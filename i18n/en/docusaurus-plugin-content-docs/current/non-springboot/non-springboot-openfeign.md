---
id: non-springboot-openfeign
title: SpringCloud's Openfeign
sidebar_label: SpringCloud's Openfeign
---

If your RPC is spring cloud, you need to configure the following in spring xml

```xml
<bean class="com.yomahub.tlog.feign.filter.TLogFeignFilter"/>
<bean class="com.yomahub.tlog.core.aop.AspectLogAop"/>
```

At the same time, the following configuration needs to be done in the spring mvc xml
```xml
<mvc:interceptors>
    <bean class="com.yomahub.tlog.web.interceptor.TLogWebInterceptor" />
</mvc:interceptors>
```