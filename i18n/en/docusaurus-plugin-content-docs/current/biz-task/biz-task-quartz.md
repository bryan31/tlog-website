---
id: biz-task-quartz
title: Quartz framework support
sidebar_label: Quartz framework support
---

Starting from version 1.3.0, TLog supports the Spring Quartz framework

Just replace `QuartzJobBean` with `TLogQuartzJobBean`.

Example:

```java {1}
public class DateTimeJob extends TLogQuartzJobBean {

     private Logger log = LoggerFactory.getLogger(this.getClass());

     @Override
     public void executeTask(JobExecutionContext jobExecutionContext) throws JobExecutionException {
         //Get the associated data in JobDetail
         String msg = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("msg");
         log.info("current time :"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "---" + msg);
     }
}
```