---
id: biz-task-jdk
title: JDK TIMER task
sidebar_label: JDK TIMER task
---

Starting from version 1.3.0, TLog supports the timing tasks of JDK Timer.

Just replace `TimerTask` with `TLogTimerTask`

Example:

```java {2}
Timer timer = new Timer();
timer.scheduleAtFixedRate(new TLogTimerTask() {
    @Override
    public void runTask() {
        log.info("hello,world");
    }
}, 100, 1000);
```