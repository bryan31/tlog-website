---
id: biz-task-spring-scheduled
title: spring-scheduled support
sidebar_label: spring-scheduled support
---

Starting with version 1.3.4, TLog has support for @Scheduled in Spring.

In springBoot, you don't need to change anything. You only need to import dependent packages to take effect.

With Spring Native, you need an extra line

```xml
<bean class="com.yomahub.tlog.task.spring.SpringScheduledTaskAop"/>
` ` `