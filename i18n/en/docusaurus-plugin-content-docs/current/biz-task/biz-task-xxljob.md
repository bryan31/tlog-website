---
id: biz-task-xxljob
title: XXL-JOB framework support
sidebar_label: XXL-JOB framework support
---

Starting with version 1.3.0, TLog supports the open source framework XXL-Job.

In springBoot, you don't need to change anything. You only need to import dependent packages to take effect.

In Spring Native, you need to configure an extra line

```xml
<bean class="com.yomahub.tlog.xxljob.aop.TLogXxlJobAop"/>
```