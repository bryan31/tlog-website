---
id: mq
title: Support for mq middleware
sidebar_label: Support for mq middleware
---



At present, Tlog's support for message middleware is intrusive, and it is temporarily impossible to be completely non-invasive.

For the client, you only need to wrap your business bean with `TLogMqWrapBean`.

```java
TLogMqWrapBean<BizBean> tLogMqWrap = new TLogMqWrapBean(bizBean);
mqClient.send(tLogMqWrap);
```



For the consumer side, you need to do this:

```java
//Receive tLogMqWrapBean from mq
TLogMqConsumerProcessor.process(tLogMqWrapBean, new TLogMqRunner<BizBean>() {
     @Override
     public void mqConsume(BizBean o) {
//Business operations
     }
});
```

