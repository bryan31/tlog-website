---
id: custom-label-dot-operation
title: Dot operator
sidebar_label: Dot operator
---

`@TLogAspect` supports the dot operator, which is suitable for the value of the object, and supports the types:

- Bean object
- Map object
- String in Json format
- Fastjson's JSONObject object



```java
@TLogAspect({"person.id","person.age","person.company.department.dptId"})
public void demo(Person person){
   log.info("Multi-parameter plus multi-level example");
}
```

The log output looks like the following, where the frame spanId+traceId is in front:

```
2020-02-08 22:09:40.110 [main] INFO Demo-<0.2><7205781616706048>[31-25-80013] Multi-parameter plus multi-level example
```



Of course, you can also use the dot operator to manipulate strings that conform to the Json format

```java
@TLogAspect({"person.id","person.age","person.company.department.dptId"})
public void demo(String person){
   log.info("Multi-parameter plus multi-level example");
}
```

 

For the array in the Json format string, you can even use the subscript `[num]` to access

```java
@TLogAspect({"person.id","person.age","person.company.title[1]"})
public void demo(String person){
   log.info("Multi-parameter plus multi-level example");
}
```

