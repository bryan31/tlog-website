---
id: custom-label-multi-args
title: Multiple parameters
sidebar_label: Multiple parameters
---

`@TLogAspect` annotation supports multiple parameters:

```java
@TLogAspect({"id","name"})
public void demo1(String id,String name){
   log.info("This is the first log");
   log.info("This is the second log");
   log.info("This is the third log");
   new Thread(() -> log.info("This is an asynchronous log")).start();
}
```

Assuming that the value of the incoming id is'NO1234' and the name is'jenny', the log will look like the following, where the front is the frame spanId+traceId:

```
2020-02-08 22:09:40.101 [main] INFO Demo-<0.2><7205781616706048>[NO1234-jenny] This is the first log
2020-02-08 22:09:40.101 [main] INFO Demo-<0.2><7205781616706048>[NO1234-jenny] This is the second log
2020-02-08 22:09:40.102 [main] INFO Demo-<0.2><7205781616706048>[NO1234-jenny] This is the third log
2020-02-08 22:09:40.103 [Thread-3] INFO Demo-<0.2><7205781616706048>[NO1234-jenny] This is an asynchronous log
```
