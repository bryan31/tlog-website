---
id: custom-label-multi-joint
title: Multi-value connector
sidebar_label: Multi-value connector
---

`@TLogAspect` supports custom patterns and multiple parameter connectors:

```java
@TLogAspect(value = {"id","name"},pattern = "<-{}->",joint = "_")
public void demo(String id,String name){
   log.info("Added patter and joint example");
}
```

The log output looks like the following, where the frame spanId+traceId is in front:

```
2020-02-08 22:09:40.103 [main] INFO Demo-<0.2><7205781616706048><-NO1234_jenny-> Example with patter and joint added
```
