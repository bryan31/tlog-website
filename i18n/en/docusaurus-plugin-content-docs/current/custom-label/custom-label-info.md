---
id: custom-label-info
title: What can business custom labels do
sidebar_label: What can business custom labels do
---

:::info
When the system of many companies is logging, each log will bring some business information, such as record ID, member CODE, to facilitate the positioning of the business log. Now with TLog, you can not only add distributed link labels, but also automatically add business labels for you. This makes it easier to search when locating the log.

Tlog supports custom business tags at the method level. You can define simple annotations on the method to realize the unified addition of business indicator tags in the log of a certain method for more detailed positioning.
:::
