---
id: custom-label-str
title: Constant string tag
sidebar_label: Constant string tag
---

The '@TLogAspect' annotation also supports constant strings as tags, which you can set as follows:

```java
@TLogAspect(str = "XYZ")
public void demo1(String name){
  log.info("This is the first log");
  log.info("This is the second log");
  log.info("This is the third log");
  new Thread(() -> log.info("This is asynchronous logging")).start();
}
```

```
2020-02-08 20:22:33.945 [main] INFO  Demo - <0.2><7205781616706048>[XYZ] This is the first log
2020-02-08 20:22:33.945 [main] INFO  Demo - <0.2><7205781616706048>[XYZ] This is the second log
2020-02-08 20:22:33.945 [main] INFO  Demo - <0.2><7205781616706048>[XYZ] This is the third log
2020-02-08 20:22:33.948 [Thread-3] INFO  Demo - <0.2><7205781616706048>[XYZ] This is asynchronous logging
```