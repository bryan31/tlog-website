---
id: custom-label-convertor
title: Custom Converter
sidebar_label: Custom Converter
---

`@TLogAspect` supports custom Convert, suitable for more complex business scenarios

```java
@TLogAspect(convert = CustomAspectLogConvert.class)
public void demo(Person person){
   log.info("Custom Convert Example");
}
```

```java
public class CustomAspectLogConvert implements AspectLogConvert {
     @Override
     public String convert(Object[] args) {
         Person person = (Person)args[0];
         return "PERSON(" + person.getId() + ")";
     }
}
```

The printed log looks like the following, where the front is the frame spanId+traceId:

```
2020-02-20 17:05:12.414 [main] INFO Demo-<0.2><7205781616706048>[PERSON(31] Custom Convert Example
```
