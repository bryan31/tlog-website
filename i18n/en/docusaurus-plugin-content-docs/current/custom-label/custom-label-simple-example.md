---
id: custom-label-simple-example
title: Simple example
sidebar_label: Simple example
---

Add `@TLogAspect` annotation to your method. A simple example is as follows:

```java
@TLogAspect({"id"})
public void demo1(String id,String name){
   log.info("This is the first log");
   log.info("This is the second log");
   log.info("This is the third log");
   new Thread(() -> log.info("This is an asynchronous log")).start();
}
```

Assuming the id value is'NO1234', the log output looks like the following, where the front is the frame spanId+traceId

```
2020-02-08 20:22:33.945 [main] INFO Demo-<0.2><7205781616706048>[NO1234] This is the first log
2020-02-08 20:22:33.945 [main] INFO Demo-<0.2><7205781616706048>[NO1234] This is the second log
2020-02-08 20:22:33.945 [main] INFO Demo-<0.2><7205781616706048>[NO1234] This is the third log
2020-02-08 20:22:33.948 [Thread-3] INFO Demo-<0.2><7205781616706048>[NO1234] This is an asynchronous log
```
