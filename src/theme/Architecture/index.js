import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import {translate} from '@docusaurus/Translate';
import { FiCheck, FiX } from "react-icons/fi";

import Headline from "@theme/Headline";
import styles from "./styles.module.scss";

const headline_value = {
  category: translate({
      id: 'architecture.headline_value.category',
      message: '结构简单，但能精准解决中小型公司的痛点',
    }),
  title: translate({
      id: 'architecture.headline_value.title',
      message: 'TLog的架构图',
    }),
  subtitle: translate({
      id: 'architecture.headline_value.subtitle',
      message: 'TLog的架构图通俗易懂，提供3种接入方式，大部分的注入和加载都在启动期完成。对性能无影响。',
    }),
};

function Architecture() {
  const context = useDocusaurusContext();
  const { i18n = {} } = context;
  const { currentLocale, defaultLocale } = i18n;
  var arch_img = "img/architecture"
  if (currentLocale == defaultLocale) {
    arch_img = arch_img + ".png";
  } else {
    arch_img = arch_img + "_" + currentLocale + ".png";
  }
  return (
    <>
      <section id="architecture" className={styles.architecture}>
        <div className="container">
          <div className="row">
            <div className="col col--4 col--offset-1">
              <Headline
                  category={headline_value.category}
                  title={headline_value.title}
                  subtitle={headline_value.subtitle}
              />
            </div>
            <div className="col col--6 col--offset-1">
              <img src={arch_img}/>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Architecture;
