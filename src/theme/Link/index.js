import React from "react";
import clsx from 'clsx';
import {translate} from '@docusaurus/Translate';
import { FiCheck, FiX } from "react-icons/fi";

import Headline from "@theme/Headline";
import styles from "./styles.module.scss";

const headline_value = {
  category: translate({
      id: 'link.headline_value.category',
      message: '友情链接',
    }),
};


function Link() {
  return (
    <>
      <section id="link" className={styles.link}>
        <div className="container">
          <div className="row">
            <div className="col col--10 col--offset-1">
              <Headline
                  category={headline_value.category}
              />
            </div>
          </div>
          <div className={clsx('row', styles.linksLogo)}>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com">
                <img src="img/link/gitee-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://www.oschina.net">
                <img src="img/link/oschina-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://dromara.org/">
                <img src="img/link/dromara-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/liteFlow">
                <img src="img/link/liteflow-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/hutool">
                <img src="img/link/hutool-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/hmily">
                <img src="img/link/hmily-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://dromara.org/projects/raincat/overview/">
                <img src="img/link/raincat-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/sureness">
                <img src="img/link/sureness-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/sa-token">
                <img src="img/link/satoken-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/myth">
                <img src="img/link/myth-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/forest">
                <img src="img/link/forest-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dromara/MaxKey">
                <img src="img/link/maxkey-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="https://gitee.com/dotnetchina">
                <img src="img/link/dotnet-china-logo.png"/>
              </a>
            </div>
            <div className="col col--2 col--offset-1">
              <a href="http://www.pearadmin.com/">
                <img src="img/link/pearAdmin-logo.png"/>
              </a>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Link;
