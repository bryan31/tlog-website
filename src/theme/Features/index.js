import React from "react";
import clsx from "clsx";
import Translate, {translate} from '@docusaurus/Translate';
import { FiCoffee, FiDatabase, FiPackage, FiZap } from "react-icons/fi";

import styles from "./styles.module.scss";

const size = 24;
const data = [
  {
    icon: <FiPackage size={size} />,
    title: translate({
      id: 'features.data.title_first',
      message: '轻量级，但是很强大',
    }),
    description: (
      <Translate id="features.data.description_first">
        TLog通过对日志打标签完成企业级微服务的日志追踪。它不收集日志，使用简单，
        产生全局唯一追踪码。除了追踪码以外，TLog还支持SpanId和上下游服务信息
        标签的追加。你还可以自定义方法级别的标签，让日志的定位轻而易举。
      </Translate>
    ),
  },
  {
    icon: <FiCoffee size={size} />,
    title: translate({
      id: 'features.data.title_second',
      message: '10分钟即可接入TLog',
    }),
    description: (
      <Translate id="features.data.description_second">
        为用户使用方便而设计，提供完全零侵入式接入方式，自动探测项目中使用的RPC框架和日志框架，
        进行字节码的注入完成系统级日志标签的追加。你的项目用不到10分钟即可接入TLog。
      </Translate>
    ),
  },
  {
    icon: <FiDatabase size={size} />,
    title:  translate({
      id: 'features.data.title_third',
      message: '适配主流RPC和日志框架',
    }),
    description: (
      <Translate id="features.data.description_third">
        TLog适配了市面上主流的RPC框架：dubbo，dubbox，spring cloud的open feign。
        同时适配了三大主流Log框架：log4j,logback,log4j2。支持springboot(1.5.X~2.X)
      </Translate>
    ),
  },
  {
      icon: <FiZap size={size} />,
      title:  translate({
      id: 'features.data.title_fourth',
      message: '稳定，快速',
    }),
      description: (
       <Translate id="features.data.description_fourth">
         TLog提供Javaagent，字节码注入，日志框架适配三种接入模式，无论是哪一种，都保证了无性能损耗。支持在业务异步线程，线程池，
         日志异步输出这几种场景下追踪不中断。
       </Translate>
      ),
  },
];

function Feature({ icon, title, description }) {
  return (
    <div className={clsx("col col--6", styles.feature)}>
      <div className="item">
        <div className={styles.header}>
          {icon && <div className={styles.icon}>{icon}</div>}
          <h2 className={styles.title}>{title}</h2>
        </div>
        <p>{description}</p>
      </div>
    </div>
  );
}

function Features() {
  return (
    <>
      {data && data.length && (
        <section id="features" className={styles.features}>
          <div className="container">
            <div className="row">
              <div className="col col--11 col--offset-1">
                <div className="row">
                  {data.map((props, idx) => (
                    <Feature key={idx} {...props} />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}

export default Features;
