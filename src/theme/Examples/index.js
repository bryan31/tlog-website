import React from "react";
import {translate} from '@docusaurus/Translate';
import CodeSnippet from "@theme/CodeSnippet";
import Tabs from "@theme/Tabs";
import TabItem from "@theme/TabItem";

import Headline from "@theme/Headline";
import snippets from "./snippets";
import styles from "./styles.module.scss";

const headline_value = {
  category: translate({
      id: 'examples.headline_value.category',
      message: '无侵入极速接入',
    }),
  title: translate({
      id: 'examples.headline_value.title',
      message: '在springboot环境下最快1分钟即可接入TLog',
    }),
};

function renderTabs() {
  return (
    <Tabs
      defaultValue={snippets[0].label}
      values={snippets.map((props, idx) => {
        return { label: props.label, value: props.label };
      })}
      className={styles.tabs}
    >
      {snippets.map((props, idx) => (
        <TabItem key={idx} value={props.label}>
          <CodeSnippet key={idx} {...props} />
        </TabItem>
      ))}
    </Tabs>
  );
}

function Examples() {
  return (
    <>
      {snippets && snippets.length && (
        <section id="examples" className={styles.examples}>
          <div className="container">
            <div className="row">
              <div className="col col--10 col--offset-1">
                <Headline
                  category={headline_value.category}
                  title={headline_value.title}
                />
                {renderTabs()}
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}

export default Examples;
