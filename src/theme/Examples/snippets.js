import {translate} from '@docusaurus/Translate';
const snippets = [
  {
    language: 'html',
    label: translate({
        id: 'examples.snippets.label1',
        message: "STEP1-下载agent包"
      }),
    code: translate({
        id: 'examples.snippets.code1',
        message: "访问：https://gitee.com/dromara/TLog/releases/v1.4.3 下载tlog-agent.jar",
      }),
  },
  {
    language: 'shell',
    label: translate({
        id: 'examples.snippets.label2',
        message: "STEP2-在jvm参数中加入"
      }),
    code: translate({
        id: 'examples.snippets.code2',
        message: "-javaagent:/your_path/tlog-agent.jar",
      }),
  },
  {
    language: 'log',
    label: translate({
        id: 'examples.snippets.label3',
        message: "效果-消费者端"
      }),
    code: `2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> logback-dubbox-consumer:invoke method sayHello,name=jack  >> com.yomahub.tlog.example.dubbox.controller.DemoController:22
2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> 测试日志aaaa  >> com.yomahub.tlog.example.dubbox.controller.DemoController:23
2020-09-16 18:12:56,763 [INFO] <0><7161457983341056> 测试日志bbbb  >> com.yomahub.tlog.example.dubbox.controller.DemoController:24`,
  },
  {
    language: 'log',
    label: translate({
        id: 'examples.snippets.label4',
        message: "效果-提供者端"
      }),
    code: `2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> logback-dubbox-provider:invoke method sayHello,name=jack  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:15
2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> 测试日志cccc  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:16
2020-09-16 18:12:56,854 [INFO] <0.1><7161457983341056> 测试日志dddd  >> com.yomahub.tlog.example.dubbo.service.impl.DemoServiceImpl:17`,
  },
];

export default snippets;
