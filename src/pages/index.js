import "./index.css";
import React, { useState } from "react";
import { IconContext } from "react-icons";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import styles from "./styles.module.scss";

// Prism (Rust)
import Prism from "prism-react-renderer/prism";
(typeof global !== "undefined" ? global : window).Prism = Prism;
require("prismjs/components/prism-rust");

import Architecture from "@theme/Architecture";
import Examples from "@theme/Examples";
import Features from "@theme/Features";
import Hero from "@theme/Hero";
import Link from "@theme/Link";

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  const { tagline } = siteConfig;

  return (
    <IconContext.Provider value={{ style: { verticalAlign: "middle" } }}>
      <Layout description={tagline}>
        <Hero />

        <main className={styles.main}>
          <Features />
          <Architecture />
          <Examples />
          <Link />
        </main>
      </Layout>
    </IconContext.Provider>
  );
}

function Layer() {
  const storeKey = "show-notify";
  const [show, setShow] = useState(() => {
    try {
      const value = window.localStorage.getItem(storeKey);
      return !value ? true : false;
    }
    catch {
      return true;
    }
  });

  if (!show) {
    return null;
  }

  const hideModal = (ev) => {
    setShow(false);
    window.localStorage.setItem(storeKey, false);
  }

  // 点击 ok
  const handleOk = (ev) => {
    hideModal();
    window.open("https://www.oschina.net/project/top_cn_2021/?id=578");
  };

  // 点击取消
  const handleCancel = (ev) => {
    hideModal();
  }

  return <div className="layer">
    {show && <div className="layer-container animate layerAnimate">
      <div className="layer-title">为TLog进行投票</div>
      <div className="layer-content">
        我的另一个开源项目LiteFlow已经进入《2021年度OSC中国最佳开源项目评选》第二轮投票，本次评选会决出最受欢迎TOP 30，之前票数已经清零，大家重新有5票，请大家为LiteFlow投上一票，感谢各位！<a href="https://www.oschina.net/project/top_cn_2021/?id=578">https://www.oschina.net/project/top_cn_2021/?id=578</a>
        <br/><br/>
        LiteFlow官网：https://liteflow.yomahub.com
        <br/><br/>
        PS：点击 好的/残忍拒绝 后近期不再弹出此信息
      </div>
      <div className="layer-tools">
        <button className="layer-button" style={{ backgroundColor: '#FFC767', color: '#000000' }} onClick={handleOk}>好的！</button>
        <button className="layer-button" onClick={handleCancel}>残忍拒绝</button>
      </div>
    </div>}
  </div>
}

export default Home;
