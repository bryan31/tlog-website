---
id: javaagent-type-shell-config
title: Java启动参数配置
sidebar_label: Java启动参数配置
---

使用javaagent方式，只需要在你的java启动参数中加入：
```shell script
-javaagent:/your_path/tlog-agent.jar
```

最新的tlog-agent.jar可以在以下地址进行下载

https://gitee.com/dromara/TLog/releases/v1.4.3
