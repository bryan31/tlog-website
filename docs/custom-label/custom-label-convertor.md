---
id: custom-label-convertor
title: 自定义Convertor
sidebar_label: 自定义Convertor
---

`@TLogAspect`支持自定义Convert，适用于更复杂的业务场景

```java
@TLogAspect(convert = CustomAspectLogConvert.class)
public void demo(Person person){
  log.info("自定义Convert示例");
}
```

```java
public class CustomAspectLogConvert implements AspectLogConvert {
    @Override
    public String convert(Object[] args) {
        Person person = (Person)args[0];
        return "PERSON(" + person.getId() + ")";
    }
}
```

日志打印出来的样子如下，其中前面为框架spanId+traceId：

```
2020-02-20 17:05:12.414 [main] INFO  Demo - <0.2><7205781616706048>[PERSON(31] 自定义Convert示例
```
