---
id: servlet
title: 对servlet的支持
sidebar_label: 对servlet的支持
---

虽然我很好奇现在还有没有人在继续使用servlet技术，但TLog从1.3.5开始，也支持了servlet

你需要在`web.xml`声明一个filter和filter-mapping即可

```xml {3}
<filter>
    <filter-name>tlogServletFilter</filter-name>
    <filter-class>com.yomahub.tlog.web.filter.TLogServletFilter</filter-class>
</filter>

<filter-mapping>
    <filter-name>tlogServletFilter</filter-name>
    <url-pattern>*.do</url-pattern>
</filter-mapping>
```