---
id: custom_traceid
title: 自定义TraceId生成器
sidebar_label: 自定义TraceId生成器
---

TLog默认采用snowflake算法生成traceId，当然你也可以去更换traceId的生成算法。

定义自己的traceId生成类去继承`TLogIdGenerator`接口：

```java
public class TestIdGenerator extends TLogIdGenerator {
    @Override
    public String generateTraceId() {
        return String.valueOf(System.nanoTime());
    }
}
```

然后在springboot的配置类里声明：

```properties
tlog.id-generator=com.yomahub.tlog.example.dubbo.id.TestIdGenerator
```