---
id: q&a
title: Q & A
sidebar_label: Q & A
---

**<font color="ffce73">Q：为什么我的请求从controller进来，日志标签却没有打印？</font>**

**A：你的项目里如果自己定义了web拦截器，去确认下是否继承了`WebMvcConfigurationSupport`这个接口，TLog的拦截器是实现了`WebMvcConfigurer`接口，在spring mvc中`WebMvcConfigurationSupport`和`WebMvcConfigurer`是会有冲突的。**

**解决方案为：把继承`WebMvcConfigurationSupport`换成实现`WebMvcConfigurer`接口，多个`WebMvcConfigurer`是不会产生冲突的**

**<font color="ffce73">Q：我使用的是javaagent方式/字节码方式接入，为什么不起效果？</font>**

**A：javaagent方式和字节码方式在TLog中本质都是一样的，只是javaagent不用依赖而已，都是在类加载之前，通过javassit去更改字节码。但是由于不同的公司系统差异性挺大，如果有存在在TLog之前就提前加载了相关日志框架的类，就会导致标记失效**

**解决方案为：换成`日志框架适配器方式`去接入，这是目前最稳定的方式，且支持的特性也最多**

**<font color="ffce73">Q：我用log4j2，为什么日志开头会出现大量的warning信息，这正常吗？</font>**

**A：这个是正常的，因为log4j2的加载机制是插件形式，TLog需要覆盖log4j2的默认插件，所以log4j2在启动时会有warning告知mdc的占位符被替换了**

**<font color="ffce73">Q：为什么启动日志里没有标签信息？</font>**

**A：启动日志里是不打印标签信息的，就算打了也无意义，只有业务触发才会打印。**