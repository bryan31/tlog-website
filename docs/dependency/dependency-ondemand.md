---
id: dependency-ondemand
title: 按需依赖
sidebar_label: 按需依赖
---

如果你不想依赖不必要的包，TLog对springboot提供了按需依赖

模板形式为:

```xml
<dependency>
  <groupId>com.yomahub</groupId>
  <artifactId>tlog-XXX-spring-boot-starter</artifactId>
  <version>1.4.3</version>
</dependency>
```



具体模块和描述如下表

| 模块名                                                       | 描述                                  |
| ------------------------------------------------------------ | ------------------------------------- |
| <font color="ffce73">**tlog-dubbo-spring-boot-starter**</font> | 适用于apache dubbo的项目              |
| <font color="ffce73">**tlog-dubbox-spring-boot-starter**</font> | 适用于当当的dubbox的项目              |
| <font color="ffce73">**tlog-feign-spring-boot-starter**</font> | 适用于spring cloud中open feign的项目  |
| <font color="ffce73">**tlog-gateway-spring-boot-starter**</font> | 适用于spring cloud中的gateway网关服务 |
| <font color="ffce73">**tlog-soul-spring-boot-starter**</font> | 适用于soul网关服务                    |
| <font color="ffce73">**tlog-web-spring-boot-starter**</font> | 适用于有spring web的项目              |
| <font color="ffce73">**tlog-xxljob-spring-boot-starter**</font> | 适用于xxl-job的项目                   |


:::info
所有模块均包含log4j,log4j2,logback三大日志框架的支持，这些模块也可以组合起来使用，例如spring cloud openfeign同时需要tlog-feign-spring-boot-starter和tlog-web-spring-boot-starter
:::