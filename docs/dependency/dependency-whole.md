---
id: dependency-whole
title: 全量依赖
sidebar_label: 全量依赖
---

TLog对springboot和spring native提供了2种不同的依赖，此种方式只需依赖一个包，必须的包会传递依赖进来

**springboot依赖**

```xml
<dependency>
  <groupId>com.yomahub</groupId>
  <artifactId>tlog-all-spring-boot-starter</artifactId>
  <version>1.4.3</version>
</dependency>
```

**spring native依赖**
```xml
<dependency>
  <groupId>com.yomahub</groupId>
  <artifactId>tlog-all</artifactId>
  <version>1.4.3</version>
</dependency>
```