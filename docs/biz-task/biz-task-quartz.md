---
id: biz-task-quartz
title: Quartz框架的支持
sidebar_label: Quartz框架的支持
---

从1.3.0版本开始，TLog对Spring Quartz框架作了支持

只需要把`QuartzJobBean`替换成`TLogQuartzJobBean`就可以了。

示例：

```java {1}
public class DateTimeJob extends TLogQuartzJobBean {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void executeTask(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //获取JobDetail中关联的数据
        String msg = (String) jobExecutionContext.getJobDetail().getJobDataMap().get("msg");
        log.info("current time :"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "---" + msg);
    }
}
```