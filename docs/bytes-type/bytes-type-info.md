---
id: bytes-type-info
title: 字节码注入方式
sidebar_label: 字节码注入方式
---

:::info
这种方式适合springboot项目，需要项目依赖tlog-all-spring-boot-starter包，
tlog提供springboot的自动装配功能。如果你的项目不是springboot项目，请参照`日志框架适配方式`
:::

:::caution
这种方式和javaagent接入差不多，虽然简单，但是不支持MDC和异步日志。而且只支持springboot项目。
:::