---
id: okhttp
title: 对Okhttp的支持
sidebar_label: 对Okhttp的支持
---

目前TLog对于Okhttp客户端的支持是有侵入性的，对于请求端需要加入拦截器，把日志标签信息记录用过Http Header传递到服务器。

添加拦截器`TLogOkHttpInterceptor`

```java
    String url = "http://127.0.0.1:2111/hi?name=2323";
    //创建OkHttpClient请求对象
    OkHttpClient client = new OkHttpClient().newBuilder()
            .addInterceptor(new TLogOkHttpInterceptor())
            .build();
    //创建Request
    Request request = new Request.Builder().url(url).build();
    //得到Call对象
    Call call = client.newCall(request);
    try {
        Response response = call.execute();
        log.info("http response result:{}",response.body().string());
    } catch (IOException e) {
        e.printStackTrace();
    }
```