---
id: soul
title: 对Soul网关的支持
sidebar_label: 对Soul网关的支持
---

TLog从1.3.0开始，对开源框架Soul网关也进行了支持

你只需在gateway的server项目中引入`tlog-web-spring-boot-starter`和`tlog-soul-spring-boot-starter`模块即可

TLog会在gateway启动时进行自动装载适配，具体如何依赖请参照`安装TLog / 按需依赖`