---
id: resttemplate
title: 对RestTemplate的支持
sidebar_label: 对RestTemplate的支持
---

TLog从1.3.6开始，Spring Web中的RestTemplate也进行了支持。

你需要把`TLogRestTemplateInterceptor`这个拦截器配置到相关地方皆可，例如：

```java
restTemplate.setInterceptors(Collections.singletonList(new TLogRestTemplateInterceptor()));
```
