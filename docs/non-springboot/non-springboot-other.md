---
id: non-springboot-other
title: 其他配置
sidebar_label: 其他配置
---

如果你要自定义模板显示和开启rpc时间打印显示，需要在spring xml如下配置

```xml
<bean class="com.yomahub.tlog.spring.TLogPropertyInit">
    <property name="pattern" value="[$preApp][$preHost][$preIp][$spanId][$traceId]"/>
    <property name="enableInvokeTimePrint" value="true"/>
</bean>
```