---
id: non-springboot-openfeign
title: SpringCloud的Openfeign
sidebar_label: SpringCloud的Openfeign
---

如果你的RPC是spring cloud，需要在spring xml里如下配置

```xml
<bean class="com.yomahub.tlog.feign.filter.TLogFeignFilter"/>
<bean class="com.yomahub.tlog.core.aop.AspectLogAop"/>
```

同时需要在spring mvc的xml里做如下配置
```xml
<mvc:interceptors>
    <bean class="com.yomahub.tlog.web.interceptor.TLogWebInterceptor" />
</mvc:interceptors>
```