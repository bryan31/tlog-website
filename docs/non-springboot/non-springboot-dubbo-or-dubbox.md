---
id: non-springboot-dubbo-or-dubbox
title: dubbo & dubbox
sidebar_label: dubbo & dubbox
---

如果你的RPC是dubbo或者dubbox，需要在spring xml里如下配置

```xml
<bean class="com.yomahub.tlog.web.TLogWebConfig"/>
<bean class="com.yomahub.tlog.core.aop.AspectLogAop"/>
```

同时需要在spring mvc的xml里做如下配置
```xml
<mvc:interceptors>
    <bean class="com.yomahub.tlog.web.interceptor.TLogWebInterceptor" />
</mvc:interceptors>
```
